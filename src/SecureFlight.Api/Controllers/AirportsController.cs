using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class AirportsController : SecureFlightBaseController
{
    private readonly IRepository<Airport> airportRepository;
    private readonly IService<Airport> airportService;

    public AirportsController(
        IRepository<Airport> airportRepository,
        IService<Airport> airportService,
        IMapper mapper)
        : base(mapper)
    {
        this.airportRepository = airportRepository;
        this.airportService = airportService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<AirportDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var airports = await airportService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Airport>, IReadOnlyList<AirportDataTransferObject>>(airports);
    }
    
    [HttpPut("{code}")]
    [ProducesResponseType(typeof(AirportDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Put([FromRoute] string code, AirportDataTransferObject airportDto)
    {
        var airports = new Airport
        {
            City = airportDto.City,
            Code = airportDto.Code,
            Country = airportDto.Country,
            Name = airportDto.Name,
        };

        var entity = await airportService.FindAsync(code);

        entity.Result.Code = airportDto.Code;
        entity.Result.Name = airportDto.Name;
        entity.Result.Country = airportDto.Country;
        entity.Result.City = airportDto.City;

        var result = await airportService.UpdateAsync(entity.Result, code);

        return MapResultToDataTransferObject<Airport, AirportDataTransferObject>(result);
    }
}