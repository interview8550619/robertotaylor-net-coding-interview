﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> flightService;
    private readonly IService<Passenger> passengerService;

    public FlightsController(IService<Flight> flightService, IService<Passenger> passengerService, IMapper mapper) : base(mapper)
    {
        this.flightService = flightService;
        this.passengerService = passengerService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet("ByOriginAndDestination")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetByOriginAndDestination([FromQuery] string origin, [FromQuery] string destination)
    {
        var flights = await flightService.FilterAsync(f =>
            f.DestinationAirport.Equals(destination) && f.OriginAirport.Equals(origin));

        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPut("AddPassenger")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassenger([FromQuery] string passengerId, [FromQuery] long flightId)
    {

        var passenger = await passengerService.FindAsync(passengerId);
        var flight = await flightService.FindAsync(flightId);

        // Agregar validationes si existe o no.

        flight.Result.Passengers.Add(passenger);

        var response = await flightService.UpdateAsync(flight.Result, flightId.ToString());

        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(response);
    }
}